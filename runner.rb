# frozen_string_literal: true

require_relative 'engine'

e = Engine.new
e.run
